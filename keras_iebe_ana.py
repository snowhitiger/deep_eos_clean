#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Sat 15 Oct 2016 05:55:00 AM CST

from keras.optimizers import SGD, Adam, Adamax
from keras.models import model_from_json

import numpy as np
import spec

import tensorflow as tf
tf.python.control_flow_ops = tf

# load json and create model
json_file = open('ckpt/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model

loaded_model.load_weights("ckpt/weights.hdf5")

adm = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
loaded_model.compile(loss='categorical_crossentropy',
              optimizer=adm,
              metrics=['accuracy'])


x_test, y_test, params = spec.load_data('data/test_iebevishnu.csv', return_options=True)

n_test = len(x_test)

height, width = 15, 48

x_test = x_test.reshape(n_test, height, width, 1).astype('float32')

#err, acc = loaded_model.evaluate(x_test, y_test, batch_size=1)

#print('predict err=%s, acc=%s'%(err, acc))

classes = loaded_model.predict_classes(x_test)

y_true = np.argmax(y_test, axis=1)
y_pred = classes


params_set = set(params)

print params_set


for key in params_set:
    ids = []
    for idx, param in enumerate(params):
        if key == param:
            ids.append(idx)
    print(key)
    correct_ones = np.count_nonzero(y_true[ids] == y_pred[ids])
    total = len(ids)
    print(correct_ones, ' out of ', total, 'is correct', 'ratio=', correct_ones/float(total))
    print('')

### compute accuracy_vs_etaos
import re
acc_vs_etaos = []
for key in params_set:
    ids = []
    for idx, param in enumerate(params):
        if key == param:
            ids.append(idx)
    print(key)
    correct_ones = np.count_nonzero(y_true[ids] == y_pred[ids])
    total = len(ids)
    match = re.search(r"_(?P<etaos>\d+)eta", key)
    if not match:
        etav = '0.0'
    else:
        match = match.group(1)
        etav = match[0] + '.' + match[1:]
    acc_vs_etaos.append([etav, correct_ones, total])
    print(correct_ones, ' out of ', total, 'is correct', 'ratio=', correct_ones/float(total))
    print('')

res = []
for acc in acc_vs_etaos:
    etaos = float(acc[0])
    label = '%.3f'%etaos
    res.append([label, acc[1], acc[2]])

import pandas as pd

print res
df = pd.DataFrame({'etaos':[acc[0] for acc in res],
                  'positive':[acc[1] for acc in res],
                  'total':[acc[2] for acc in res]})

print df.groupby(by='etaos').sum()

