#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Fri 30 Sep 2016 11:41:54 AM CEST

import numpy as np
import cmath
import four_momentum as mom
from four_momentum import phi_integral, pt_integral
from augmentation import random_shift
import pandas as pd

NPT, NPHI = mom.NPT, mom.NPHI

class Dataset(object):
    def __init__(self, images, labels, one_hot=False):
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0
        if images.shape[0] == labels.shape[0]:
            self._num_of_samples = images.shape[0]
        print(self._num_of_samples)

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_of_samples(self):
        return self._num_of_samples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size):
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_of_samples:
            self._epochs_completed += 1
            print('epoch:%s finished!'%self._epochs_completed)
            pmut = np.arange(self._num_of_samples)
            np.random.shuffle(pmut)
            self._images = self._images[pmut]
            self._labels = self._labels[pmut]
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size < self._num_of_samples
        end = self._index_in_epoch
        return self.images[start:end], self.labels[start:end]


    
def mean_pt(spec):
    '''calc the <pt> and multiplicity from
    Args:
      spec: size=npt*nphi, dN/(2pi dY pt dpt dphi)
    Return:
      the <pt> dN/dY and pt spectra at mid-rapidity'''
    spec1 = spec.reshape(NPT, NPHI)
    pt = mom.PT
    dn_dyptdpt = np.apply_along_axis(arr=spec1,
                                     func1d=phi_integral, axis=1)
    dndy = pt_integral(pt*dn_dyptdpt)
    ptdndy = pt_integral(pt*pt*dn_dyptdpt)
    return ptdndy/dndy, dndy, dn_dyptdpt


def vn_psin(spec, n=2):
    '''calc pt integrated vn and event plane psi_n
    Args:
      spec: size=npt*nphi, dN/(2pi dY pt dpt dphi)
    Return:
      the <pt> , dN/dY at mid-rapidity'''

    spec1 = spec.reshape(NPT, NPHI)
    spec_along_phi = np.apply_along_axis(arr=spec1,
                                        func1d=pt_integral, axis=0)
    norm = phi_integral(spec_along_phi)
    
    vn_int, psi_n = cmath.polar(phi_integral(
                         spec_along_phi*np.exp(1j*n*mom.PHI))/norm)    
    return vn_int, psi_n/float(n)


def vn_vs_pt(spec, event_plane):
    ''' calc pt differential vn(n=2, 3, 4, 5)  with given spec
        and event plane angle.'''
    spec1 = spec.reshape(NPT, NPHI)
    Vn_vs_pt = np.zeros(shape=(4, NPT))
    angles = np.zeros(shape=(4, NPT))
    PHI = mom.PHI

    for i in range(NPT):
        spec_along_phi = spec1[i,:]
        norm_factor = phi_integral(spec_along_phi)
        for n in xrange(0, 4):
            Vn_vs_pt[n, i], angles[n, i] = cmath.polar(phi_integral(
                spec_along_phi*np.exp(1j*(n+2)*(PHI-event_plane[n])))
                /norm_factor)

    return Vn_vs_pt.flatten()



def ordinary_observables(x_dat):
    '''get the ordinary_observables from spec
    Return:
        <pt>, npi, v2, v3, v4, v5, psi_2, psi_3, psi_4, psi_5
    '''
    hbarc3 = 0.19732**3.0
    observables = []
    for idx, spec in enumerate(x_dat):
        spec = spec / hbarc3
        #spec = spec/spec.max()
        mean_pt_i, dndy_i, pt_spec = mean_pt(spec)
        v2_i, psi2_i = vn_psin(spec, n=2)
        v3_i, psi3_i = vn_psin(spec, n=3)
        v4_i, psi4_i = vn_psin(spec, n=4)
        v5_i, psi5_i = vn_psin(spec, n=5)
        line = [mean_pt_i, dndy_i, v2_i, v3_i, v4_i, 
                            v5_i, psi2_i, psi3_i, psi4_i, psi5_i]
        for spi in pt_spec: line.append(spi)

        vn_pt = vn_vs_pt(spec, event_plane=[psi2_i, psi3_i, psi4_i, psi5_i])

        for vn in vn_pt: line.append(vn)

        observables.append(line)
        print(idx,)

    observables = np.array(observables)
    return observables

from augmentation import skip_npt

size_of_image = (NPT - skip_npt) * NPHI

size_of_labels = 2


# change y to vector representation
def regulize_y(y_label):
    y_dat = []
    for i in range(len(y_label)):
        if y_label[i] == 0:
            y_dat.append([1., 0.])
        else:
            y_dat.append([0., 1.])
    
    y_dat = np.array(y_dat)
    return y_dat


def normalize(data1d):
    a =  data1d.reshape(NPT, NPHI)[:NPT-skip_npt, :].flatten()
    if a.max() == 0:
      print('a.max() == 0')
    return a / a.max() - 0.5

# normalize and reshuffling the order of the input dataset
def regulize(x_dat, y_dat, shuffle=True, choose_highpt=True):
    a_dat = np.apply_along_axis(normalize, axis=1, arr=x_dat)
    y_dat = regulize_y(y_dat)
    randomize = np.arange(len(a_dat))

    if shuffle == True:
        np.random.shuffle(randomize)
        a_dat = a_dat[randomize]
        y_dat = y_dat[randomize]

    return a_dat, y_dat, randomize

# flip left right for phi direction to double the training dataset
def flip_left_right(x_dat):
    x_flip = np.copy(x_dat)
    def flip(spec1d):
        spec2d = spec1d.reshape(NPT-skip_npt, NPHI)
        return spec2d[:, ::-1].flatten()
    return np.apply_along_axis(flip, arr=x_flip, axis=1)

def load_data(csv_path, shuffle=True, lr_flip=False, return_options=False):
    dat = pd.read_csv(csv_path)
    x_origin = dat.iloc[:, range(1, 1+NPT*NPHI)].values
    y_origin = dat.loc[:, 'eos_type'].values
    x, y, randomize = regulize(x_origin, y_origin)
    opt = dat.loc[:, 'options'].values
    opt = opt[randomize]
    if lr_flip:
        x_fliplr = flip_left_right(x)
        x = np.concatenate([x, x_fliplr], axis=0)
        y = np.concatenate([y, y], axis=0)
        opt = np.concatenate([opt, opt], axis=0)
    if return_options:
      return x, y, opt
    else:
      return x, y


