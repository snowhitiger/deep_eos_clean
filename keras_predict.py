#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Sat 15 Oct 2016 05:55:00 AM CST

from keras.optimizers import SGD, Adam, Adamax
from keras.models import model_from_json
import tensorflow as tf

import numpy as np
import spec

tf.python.control_flow_ops = tf

test = 'iebe'

if test == 'iebe':
    x_test, y_test = spec.load_data('data/test_iebevishnu.csv')
elif test == 'ipglasma':
    x_test, y_test = spec.load_data('data/test_ipglasma.csv')

n_test = len(x_test)

# load json and create model
with open('ckpt/model.json', 'r') as json_file:
    loaded_model_json = json_file.read()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("ckpt/weights.hdf5")

adm = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
loaded_model.compile(loss='categorical_crossentropy',
              optimizer=adm,
              metrics=['accuracy'])

height, width = spec.NPT, spec.NPHI

x_test = x_test.reshape(n_test, height, width, 1).astype('float32')

err, acc = loaded_model.evaluate(x_test, y_test, batch_size=1)

print('predict err=%s, acc=%s'%(err, acc))

classes = loaded_model.predict_classes(x_test)

y_true = np.argmax(y_test, axis=1)
y_pred = classes

dat_sources = {'eosl': y_true == 0,
               'eosq': y_true == 1}

for key in dat_sources.keys():
    ids = dat_sources[key]
    print(key)
    correct_ones = np.count_nonzero(y_true[ids] == y_pred[ids])
    print(correct_ones, ' out of ', ids.sum(), 'is correct', 'ratio=', correct_ones/float(ids.sum()))
    print('')
