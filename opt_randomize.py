#!/usr/bin/env python

from __future__ import absolute_import
import unittest
import numpy as np

import spec

class TestOptionRandomize(unittest.TestCase):
    '''test the functionality of opt[randomnize] to see
       whether opt[] matches x and y after shuffling'''
    def test_match(self):
        x_test, y_test, opts = spec.load_data('data/test_iebevishnu.csv', return_options=True)
        labels_opt = np.array([1.0 if 'eosq' in opt else 0.0 for opt in opts])
        labels_y = np.argmax(y_test, axis=1)
        self.assertTrue(np.all(labels_opt == labels_y))

if __name__ == '__main__':
    unittest.main()
