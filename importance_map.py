import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras.models import model_from_json
from four_momentum import PT, PHI, NPT, NPHI
import spec
import os
import re

#tf.python.control_flow_ops = tf

################ data #############################

num_of_pixels = NPT * NPHI
#x_test, y_test, opts = spec.load_data('data/test_iebevishnu.csv', return_options=True)
#x_test, y_test = spec.load_data('data/test_ipglasma.csv')
x_test, y_test = spec.load_data('data/auau200_blt8_etaos0p08.csv')

n_test = len(x_test)
x_test = x_test.reshape(n_test, NPT, NPHI, 1).astype('float32')

################ sampler #############################
class MarginalSampler(object):
    '''sample pixel values from mixed events'''
    def __init__(self, X):
        self.X = X.reshape(X.shape[0], -1)
        self.num_events = X.shape[0]

    def get_samples(self, sample_indices, num_samples = 10):
        random_id = np.random.choice(self.num_events, num_samples, replace=False)
        return np.take(self.X[random_id], sample_indices, axis=1)


################ classifier ##########################
with open('ckpt/model.json', 'r') as f:
    model = model_from_json(f.read())
# load weights into new model
model.load_weights("ckpt/weights.hdf5")
model.compile(loss='categorical_crossentropy',
          optimizer='sgd',
          metrics=['accuracy'])
#### only load model weight once to speed up 

class Classifier(object):
    def __init__(self):
        self.model = model
    def predict(self, x):
        '''return the predicted class and its probability'''
        x = np.expand_dims(x, axis=0)
        probs = self.model.predict(x)[0]
        idx = np.argmax(probs)
        return idx, probs[idx]
    
################ prediction difference analysis ####

classifier = Classifier()

def odds(p, N=80000, K=2):
    p = (p*N + 1)/(N + K)
    return p/(1.0 - p)

def weight_of_evidence(x, mask, num_of_samples=10):
    xprim = np.copy(x).flatten()
    weight_evidence = np.zeros_like(xprim)
    true_label, true_prob = classifier.predict(x)
    print('true probability:', true_prob)
    for idx in range(num_of_pixels):
        sumw = 0.0
        xx = np.copy(xprim)
        sampled_pixel_values = mask.get_samples([idx], num_of_samples)
        for s in sampled_pixel_values:
            xx[idx] = s
            label, p = classifier.predict(xx.reshape(NPT, NPHI, 1))
            sumw += p
        sumw /= num_of_samples
        weight_evidence[idx] = np.log2(odds(true_prob)) - np.log2(odds(sumw))
    return true_label, weight_evidence.reshape(NPT, NPHI)

def avg_weight_evidence(nevent=1000):
    '''highlight the most important pixels for EoS classification'''
    mask = MarginalSampler(x_test)
    avg_we = np.zeros(shape=(NPT, NPHI))
    we_eosl = []
    we_eosq = []
    for idx in range(nevent):
        x0 = x_test[idx]
        label, we = weight_of_evidence(x0, mask)
        avg_we += we / nevent
        if label == 0:
            we_eosl.append(we)
        else:
            we_eosq.append(we)
        print(idx, 'finished')
    avg_we_eosl = np.mean(np.array(we_eosl), axis=0)
    avg_we_eosq = np.mean(np.array(we_eosq), axis=0)
    if not os.path.exists('results'):
        os.makedirs('results')
    np.save('results/we_eosl_list.npy', np.array(we_eosl))
    np.save('results/we_eosq_list.npy', np.array(we_eosq))
    np.savetxt('results/avg_we.dat', avg_we)
    np.savetxt('results/avg_we_eosl.dat', avg_we_eosl)
    np.savetxt('results/avg_we_eosq.dat', avg_we_eosq)



def get_etaos(opts):
    '''get eta/s from the description string of one event'''
    etaos = []
    for key in opts:
        match = re.search(r"_(?P<etaos>\d+)eta", key)
        etav = 0.0
        if match:
            match = match.group(1)
            etav = float(match[0] + '.' + match[1:])
        etaos.append(etav)
    return etaos



def avg_weight_evidence_for_iebeetas0(kind='iebe_etas0p08', etaos=0.08):
    '''highlight the most important pixels for EoS classification'''
    etaos = get_etaos(opts)
    print(etaos)
    mask = MarginalSampler(x_test)
    avg_we = np.zeros(shape=(NPT, NPHI))
    we_eosl = []
    we_eosq = []
    for idx in range(len(x_test)):
        if etaos[idx] != etaos:
            print(etaos[idx], ' skiped')
            continue
        # only compute the eta/s==0 events in IEBE-VISHNU
        x0 = x_test[idx]
        label, we = weight_of_evidence(x0, mask)
        if label == 0:
            we_eosl.append(we)
        else:
            we_eosq.append(we)
        print(idx, 'finished')
    avg_we_eosl = np.mean(np.array(we_eosl), axis=0)
    avg_we_eosq = np.mean(np.array(we_eosq), axis=0)
    if not os.path.exists('results'):
        os.makedirs('results')
    np.save('results/we_eosl_%s_list.npy'%kind, np.array(we_eosl))
    np.save('results/we_eosq_%s_list.npy'%kind, np.array(we_eosq))
    np.savetxt('results/avg_we_eosl_%s.dat'%kind, avg_we_eosl)
    np.savetxt('results/avg_we_eosq_%s.dat'%kind, avg_we_eosq)

def avg_weight_evidence_for_iebeetas0_mix(kind='iebe_etas0p08', etaos_=0.08):
    '''highlight the most important pixels for EoS classification
    using ideal events from iebe-vishnu for both Xsame and Xmixed'''
    etaos = np.array(get_etaos(opts))
    ideal_events = (etaos == etaos_)
    x_test_ideal = x_test[ideal_events]
    print(len(x_test_ideal))
    mask = MarginalSampler(x_test_ideal)
    avg_we = np.zeros(shape=(NPT, NPHI))
    we_eosl = []
    we_eosq = []
    for idx in range(len(x_test_ideal)):
        x0 = x_test_ideal[idx]
        label, we = weight_of_evidence(x0, mask)
        if label == 0:
            we_eosl.append(we)
        else:
            we_eosq.append(we)
        print(idx, 'finished')
    avg_we_eosl = np.mean(np.array(we_eosl), axis=0)
    avg_we_eosq = np.mean(np.array(we_eosq), axis=0)
    if not os.path.exists('results'):
        os.makedirs('results')
    np.save('results/we_eosl_%s_list.npy'%kind, np.array(we_eosl))
    np.save('results/we_eosq_%s_list.npy'%kind, np.array(we_eosq))
    np.savetxt('results/avg_we_eosl_%s.dat'%kind, avg_we_eosl)
    np.savetxt('results/avg_we_eosq_%s.dat'%kind, avg_we_eosq)


if __name__ == '__main__':
    avg_weight_evidence(2000)
    #avg_weight_evidence_for_iebeetas0_mix()
