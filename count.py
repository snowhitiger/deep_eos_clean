acc_vs_etaos = [['0.13', 94, 100], ['0.03', 95, 100], ['0.16', 95, 100], ['0.', 49, 50], ['0.08', 50, 50], ['0.08', 48, 50], ['0.', 96, 100], ['0.', 47, 50], ['0.01', 94, 100], ['0.08', 89, 100], ['0.09', 94, 100], ['0.01', 96, 100], ['0.05', 90, 100], ['0.05', 91, 100], ['0.0', 49, 50], ['0.', 45, 50], ['0.', 49, 50], ['0.', 48, 50], ['0.', 46, 50], ['0.08', 44, 50], ['0.', 94, 100], ['0.03', 97, 100], ['0.09', 96, 100], ['0.08', 47, 50], ['0.08', 48, 50], ['0.08', 46, 50], ['0.', 98, 100], ['0.', 49, 50], ['0.', 45, 50], ['0.16', 44, 50], ['0.08', 187, 200], ['0.16', 47, 50], ['0.1', 94, 100], ['0.16', 45, 50], ['0.16', 45, 50], ['0.16', 96, 100], ['0.16', 48, 50], ['0.16', 49, 50], ['0.01', 93, 100], ['0.16', 46, 50], ['0.08', 50, 50], ['0.08', 95, 100], ['0.16', 46, 50], ['0.1', 99, 100], ['0.0', 47, 50], ['0.16', 48, 50], ['0.08', 48, 50], ['0.08', 49, 50], ['0.', 49, 50], ['0.12', 92, 100], ['0.0', 46, 50], ['0.01', 91, 100], ['0.16', 49, 50], ['0.', 48, 50], ['0.08', 94, 95], ['0.06', 94, 100], ['0.16', 96, 100], ['0.0', 49, 50], ['0.16', 49, 50], ['0.', 95, 100], ['0.05', 94, 100], ['0.', 47, 50], ['0.07', 95, 100], ['0.', 47, 50], ['0.08', 48, 50], ['0.0', 47, 50], ['0.1', 94, 100], ['0.16', 191, 200], ['0.16', 138, 150], ['0.02', 46, 50], ['0.09', 95, 100], ['0.02', 50, 50], ['0.16', 47, 50], ['0.09', 97, 100], ['0.', 49, 50], ['0.', 47, 50], ['0.08', 236, 250], ['0.0', 48, 50], ['0.08', 93, 99], ['0.', 97, 100], ['0.08', 96, 100], ['0.1', 97, 100], ['0.08', 47, 50], ['0.16', 47, 50], ['0.08', 49, 50], ['0.0', 97, 100], ['0.15', 93, 100], ['0.08', 49, 50], ['0.08', 50, 50], ['0.', 49, 50], ['0.1', 96, 100], ['0.08', 46, 50], ['0.07', 92, 100], ['0.16', 138, 149], ['0.16', 49, 50]]

res = []

for acc in acc_vs_etaos:
    etaos = float(acc[0])
    #label = '%.3f'%etaos
    label = etaos
    res.append([label, acc[1], acc[2]])

import pandas as pd

#print res
df = pd.DataFrame({'etaos':[acc[0] for acc in res],
                  'positive':[acc[1] for acc in res],
                  'total':[acc[2] for acc in res]})

stats = df.groupby(by='etaos').sum()
print stats
print (stats['positive']/stats['total'])

#plt.plot(stats['etaos'], stats['positive']/stats['total'], 'o')

