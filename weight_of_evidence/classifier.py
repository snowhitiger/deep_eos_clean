#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Sat 15 Oct 2016 05:55:00 AM CST
from keras.models import model_from_json
import numpy as np
import spec
import tensorflow as tf
tf.python.control_flow_ops = tf

with open('model.json', 'r') as f:
    model = model_from_json(f.read())

# load weights into new model
model.load_weights("weights.hdf5")

model.compile(loss='categorical_crossentropy',
              optimizer='sgd',
              metrics=['accuracy'])

x_train = spec.x_train
y_train = spec.y_train
n_train = len(x_train)

x_test = spec.x_test
y_test = spec.y_test
n_test = len(x_test)

height, width = 15, 48

x_train = x_train.reshape(n_train, height, width, 1).astype('float32')
x_test = x_test.reshape(n_test, height, width, 1).astype('float32')


def predict_accuracy():
    err, acc = model.evaluate(x_test, y_test, batch_size=1)
    print('predict err=%s, acc=%s'%(err, acc))
    classes = model.predict_classes(x_test)
    y_true = np.argmax(y_test, axis=1)
    y_pred = classes
    dat_sources = {'eosl': y_true == 1,
                   'eosq': y_true == 0}
    for key in dat_sources.keys():
        ids = dat_sources[key]
        print(key)
        correct_ones = np.count_nonzero(y_true[ids] == y_pred[ids])
        print(correct_ones, ' out of ', ids.sum(), 'is correct', 'ratio=', correct_ones/float(ids.sum()))
        print('')

def predict(x):
    '''return the predicted class and its probability'''
    x = np.expand_dims(x, axis=0)
    probs = model.predict(x)[0]
    idx = np.argmax(probs)
    return idx, probs[idx]

if __name__ == '__main__':
    x = x_test[0]
    print predict(x)

