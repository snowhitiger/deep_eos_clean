#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Fri 30 Sep 2016 11:41:54 AM CEST

import numpy as np
import cmath
import four_momentum as mom
from four_momentum import phi_integral, pt_integral
from augmentation import random_shift

class Dataset(object):
    def __init__(self, images, labels, one_hot=False):
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0
        if images.shape[0] == labels.shape[0]:
            self._num_of_samples = images.shape[0]
        print(self._num_of_samples)

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_of_samples(self):
        return self._num_of_samples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size):
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_of_samples:
            self._epochs_completed += 1
            print('epoch:%s finished!'%self._epochs_completed)
            pmut = np.arange(self._num_of_samples)
            np.random.shuffle(pmut)
            self._images = self._images[pmut]
            self._labels = self._labels[pmut]
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size < self._num_of_samples
        end = self._index_in_epoch
        return self.images[start:end], self.labels[start:end]


NPT, NPHI = mom.NPT, mom.NPHI
    
def mean_pt(spec):
    '''calc the <pt> and multiplicity from
    Args:
      spec: size=npt*nphi, dN/(2pi dY pt dpt dphi)
    Return:
      the <pt> dN/dY and pt spectra at mid-rapidity'''
    spec1 = spec.reshape(mom.NPT, mom.NPHI)
    pt = mom.PT
    dn_dyptdpt = np.apply_along_axis(arr=spec1,
                                     func1d=phi_integral, axis=1)
    dndy = pt_integral(pt*dn_dyptdpt)
    ptdndy = pt_integral(pt*pt*dn_dyptdpt)
    return ptdndy/dndy, dndy, dn_dyptdpt


def vn_psin(spec, n=2):
    '''calc pt integrated vn and event plane psi_n
    Args:
      spec: size=npt*nphi, dN/(2pi dY pt dpt dphi)
    Return:
      the <pt> , dN/dY at mid-rapidity'''

    spec1 = spec.reshape(NPT, NPHI)
    spec_along_phi = np.apply_along_axis(arr=spec1,
                                        func1d=pt_integral, axis=0)
    norm = phi_integral(spec_along_phi)
    
    vn_int, psi_n = cmath.polar(phi_integral(
                         spec_along_phi*np.exp(1j*n*mom.PHI))/norm)    
    return vn_int, psi_n/float(n)


def vn_vs_pt(spec, event_plane):
    ''' calc pt differential vn(n=2, 3, 4, 5)  with given spec
        and event plane angle.'''
    spec1 = spec.reshape(NPT, NPHI)
    Vn_vs_pt = np.zeros(shape=(4, NPT))
    angles = np.zeros(shape=(4, NPT))
    PHI = mom.PHI

    for i in range(NPT):
        spec_along_phi = spec1[i,:]
        norm_factor = phi_integral(spec_along_phi)
        for n in xrange(0, 4):
            Vn_vs_pt[n, i], angles[n, i] = cmath.polar(phi_integral(
                spec_along_phi*np.exp(1j*(n+2)*(PHI-event_plane[n])))
                /norm_factor)

    return Vn_vs_pt.flatten()



def ordinary_observables(x_dat):
    '''get the ordinary_observables from spec
    Return:
        <pt>, npi, v2, v3, v4, v5, psi_2, psi_3, psi_4, psi_5
    '''
    hbarc3 = 0.19732**3.0
    observables = []
    for idx, spec in enumerate(x_dat):
        spec = spec / hbarc3
        #spec = spec/spec.max()
        mean_pt_i, dndy_i, pt_spec = mean_pt(spec)
        v2_i, psi2_i = vn_psin(spec, n=2)
        v3_i, psi3_i = vn_psin(spec, n=3)
        v4_i, psi4_i = vn_psin(spec, n=4)
        v5_i, psi5_i = vn_psin(spec, n=5)
        line = [mean_pt_i, dndy_i, v2_i, v3_i, v4_i, 
                            v5_i, psi2_i, psi3_i, psi4_i, psi5_i]
        for spi in pt_spec: line.append(spi)

        vn_pt = vn_vs_pt(spec, event_plane=[psi2_i, psi3_i, psi4_i, psi5_i])

        for vn in vn_pt: line.append(vn)

        observables.append(line)
        print idx,

    observables = np.array(observables)
    return observables

skip_npt = 0

npt, nphi = 15, 48

size_of_image = (npt - skip_npt) * nphi

size_of_labels = 2


# change y to vector representation
def regulize_y(y_label):
    y_dat = []
    for i in range(len(y_label)):
        if y_label[i] == 0:
            y_dat.append([0., 1.])
        else:
            y_dat.append([1., 0.])
    
    y_dat = np.array(y_dat)
    return y_dat


def normalize(data1d):
    #data1d = rotate(data1d)
    a =  data1d.reshape(npt, nphi)[skip_npt:, :].flatten()
    #return a / a.sum()
    return a / a.max() - 0.5

# normalize and reshuffling the order of the input dataset
def regulize(x_dat, y_dat, shuffle=True, choose_highpt=True):
    for idx in range(len(x_dat)):
        x_dat[idx] = normalize(x_dat[idx])

    a_dat = np.apply_along_axis(normalize, axis=1, arr=x_dat)
    y_dat = regulize_y(y_dat)

    if shuffle == True:
        randomize = np.arange(len(a_dat))
        np.random.shuffle(randomize)
        a_dat = a_dat[randomize]
        y_dat = y_dat[randomize]

    return a_dat, y_dat

# flip left right for phi direction to double the training dataset
def flip_left_right(x_dat):
    x_flip = np.copy(x_dat)

    def flip(spec1d):
        spec2d = spec1d.reshape(npt, nphi)
        return spec2d[:, ::-1].flatten()

    return np.apply_along_axis(flip, arr=x_flip, axis=1)


#x_dat0 = np.loadtxt("../dat/dY_spec_eos_mix.dat")
#y_dat0 = np.loadtxt("../dat/dY_label_eos_mix.dat")
#
##x_ideal = np.loadtxt('dat/dY_spec_auau200ideal_pbpb2p76ideal.dat')
##y_ideal = np.loadtxt('dat/dY_label_auau200ideal_pbpb2p76ideal.dat')
##
#x_tfrz = np.loadtxt('dat/dY_spec_pbpb2p76ideal_tfrz110.dat')
#y_tfrz = np.loadtxt('dat/dY_label_pbpb2p76ideal_tfrz110.dat')
##
#x_tfrz1 = np.loadtxt('dat/dY_spec_auau200ideal_tfrz110.dat')
#y_tfrz1 = np.loadtxt('dat/dY_label_auau200ideal_tfrz110.dat')
#
#x_dat0 = np.loadtxt("../dat/dY_spec_test_pbpb2p76_auau62p4_3d_mix_ideal.dat")
#y_dat0 = np.loadtxt("../dat/dY_label_test_pbpb2p76_auau62p4_3d_mix_ideal.dat")


# no etaos
#x_eosl_withmu = np.loadtxt("../dat/dY_spec_auau200_34_pbpb2760_12_ideal_nz51_withmu.dat")
#y_eosl_withmu = np.loadtxt("../dat/dY_label_auau200_34_pbpb2760_12_ideal_nz51_withmu.dat")

#x_eosl_withmu = np.loadtxt("../dat/new_nz51/dY_spec_nz51_withmu_part_etaos0p08.dat")
#y_eosl_withmu = np.loadtxt("../dat/new_nz51/dY_label_nz51_withmu_part_etaos0p08.dat")

#x_tfrz105 = np.loadtxt('dat/togpu_tfz105/dY_spec_auau200_cent10_20_tfrz105.dat')
#y_tfrz105 = np.loadtxt('dat/togpu_tfz105/dY_label_auau200_cent10_20_tfrz105.dat')

# event with idx=10918 is buggy <pt> > 3GeV, must be cleaned first. 
x_eosl_withmu = np.delete(np.loadtxt("../dat/nz51_new_11000/dY_spec_nz51_withmu_part_etaos0p08_456.dat"), 10918, axis=0)
y_eosl_withmu = np.delete(np.loadtxt("../dat/nz51_new_11000/dY_label_nz51_withmu_part_etaos0p08_456.dat"), 10918, axis=0)

x_nz51 = np.loadtxt("../dat/dY_spec_auau200_pbpb2760_ideal_nz51.dat")
y_nz51 = np.loadtxt("../dat/dY_label_auau200_pbpb2760_ideal_nz51.dat")

x_dat1 = np.loadtxt("../dat/spec_chun_lq_220events.dat")
y_dat1 = np.loadtxt("../dat/label_chun_lq_220events.dat")

#x_dat1 = np.loadtxt("../dat/spec_chun_lq_160events_neweosq.dat")
#y_dat1 = np.loadtxt("../dat/label_chun_lq_160events_neweosq.dat")

x_dat2 = np.loadtxt("../dat/spec_azhydro_2pi.dat")
y_dat2 = np.loadtxt("../dat/label_azhydro_2pi.dat")

x_dat3 = np.loadtxt("../dat/spec_chun_lq_100events_oct12.dat")
y_dat3 = np.loadtxt("../dat/label_chun_lq_100events_oct12.dat")

#x_dat4 = np.loadtxt("../dat/spec_chun_eosq_100events_rhic_lhc.dat")
#y_dat4 = np.loadtxt("../dat/label_chun_eosq_100events_rhic_lhc.dat")

# acc = 0.76
x_dat6 = np.loadtxt("../dat/spec_chun_new_eosq_50events.dat")
y_dat6 = np.loadtxt("../dat/label_chun_new_eosq_50events.dat")

x_dat7 = np.loadtxt("../dat/spec_chun_new_eosq_100events_20161021.dat")
y_dat7 = np.loadtxt("../dat/label_chun_new_eosq_100events_20161021.dat")

x_dat8 = np.loadtxt("../dat/chun20161024/spec_chun_new_eosq_vs_eosl_500events_20161024.dat")
y_dat8 = np.loadtxt("../dat/chun20161024/label_chun_new_eosq_vs_eosl_500events_20161024.dat")

x_dat9 = np.loadtxt("../dat/IPGlasma_nz51/dY_spec_IPGlasma_nz51.dat")
y_dat9 = np.loadtxt("../dat/IPGlasma_nz51/dY_label_IPGlasma_nz51.dat")


x_dat10 = np.loadtxt("../dat/chun_mean_pt_test/spec_chun_meanpt_test.dat")
y_dat10 = np.loadtxt("../dat/chun_mean_pt_test/label_chun_meanpt_test.dat")

x_dat = x_nz51
y_dat = y_nz51

#x_dat = np.concatenate((x_tfrz, x_tfrz1, x_dat, x_ideal, x_dat0, x_nz51), axis=0)
#y_dat = np.concatenate((y_tfrz, y_tfrz1, y_dat, y_ideal, y_dat0, y_nz51), axis=0)

#x_dat = np.concatenate((x_eosl_withmu, x_nz51, x_dat9), axis=0)
#y_dat = np.concatenate((y_eosl_withmu, y_nz51, y_dat9), axis=0)

x_dat = np.concatenate((x_eosl_withmu, x_nz51), axis=0)
y_dat = np.concatenate((y_eosl_withmu, y_nz51), axis=0)

x_dat1 = np.concatenate((x_dat1, x_dat3), axis=0)
y_dat1 = np.concatenate((y_dat1, y_dat3), axis=0)

x_train, y_train = regulize(x_dat, y_dat)

#### data augmentation
#x_train_fliplr = flip_left_right(x_train)
#
#x_train = np.concatenate([x_train, x_train_fliplr], axis=0)
#y_train = np.concatenate([y_train, y_train], axis=0)
#
#x_train = np.concatenate([random_shift(x_train) for i in range(5)], axis=0)
#y_train = np.concatenate([y_train for i in range(5)], axis=0)

x_test, y_test = regulize(x_dat9, y_dat9, shuffle=True)

x_pred, y_pred = regulize(x_dat1, y_dat1)

#### training, testing and predicting dataset

train_data = Dataset(x_train, y_train)

test_data = Dataset(x_test, y_test)

pred_data = Dataset(x_pred, y_pred)

