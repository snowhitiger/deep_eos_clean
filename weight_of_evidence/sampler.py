'''Replace one region of the target image with marginal/conditional sampled
pixels, to see the effect of this replacement to the class probability.
This is used to get the relevant importance of each feature to the decision
maked in CNN'''

import matplotlib.pyplot as plt
import numpy as np

class MarginalSampler(object):
    def __init__(self, X):
        self.X = X.reshape(X.shape[0], -1)

    def get_samples(self, sample_indices, num_samples = 10):
        return np.take(self.X[:num_samples], sample_indices, axis=1)



if __name__ == '__main__':
    import spec
    X_test = spec.x_test
    sampler = MarginalSampler(X_test)
    
    x = X_test[0].reshape(-1)
    ids = [(48*i + 24) for i in xrange(15)]
    samples = sampler.get_samples(ids).mean(axis=0)

    x[ids] = samples
    plt.imshow(x.reshape(15, 48))
    plt.show()

