import matplotlib.pyplot as plt
import numpy as np
import sampler
import classifier
from four_momentum import PT, PHI
from common_plotting import smash_style

num_of_pixels = classifier.width * classifier.height

def odds(p, N=80000, K=2):
    p = (p*N + 1)/(N + K)
    return p/(1.0 - p)

def weight_of_evidence(x, mask, num_of_samples=50):
    xprim = np.copy(x).flatten()
    weight_evidence = np.zeros_like(xprim)
    true_label, true_prob = classifier.predict(x)
    print 'true probability:', true_prob
    for idx in xrange(num_of_pixels):
        sumw = 0.0
        xx = np.copy(xprim)
        sampled_pixel_values = mask.get_samples([idx], num_of_pixels)
        #sampled_pixel_values.reshape(-1)
        for s in sampled_pixel_values:
            xx[idx] = s
            label, p = classifier.predict(xx.reshape(classifier.height,
                                          classifier.width, 1))
            #print p
            sumw += p
        sumw /= num_of_samples
        weight_evidence[idx] = np.log2(odds(true_prob)) - np.log2(odds(sumw))
    return true_label, weight_evidence.reshape(classifier.height, classifier.width)


def plot_weight_evidence(idx=0):
    x_test = classifier.x_train
    mask = sampler.MarginalSampler(x_test)
    #fig, ax = plt.subplots(nrows=1, ncols=2)
    #ax[0].imshow(x0[:, :, 0], cmap='bone')
    #ax[1].imshow(we, cmap='bwr')
    fig, ax = plt.subplots(nrows=5, ncols=5)
    eos = ['EoSL', 'EoSQ']
    extent = [PHI[0], PHI[-1], PT[0], PT[-1]]
    for idx in xrange(25):
        x0 = x_test[100+idx]
        label, we = weight_of_evidence(x0, mask)
        ax[idx//5, idx%5].imshow(we, extent=extent, origin='lower', interpolation=None, cmap='viridis', aspect='auto')
        ax[idx//5, idx%5].text(np.pi, 3, '%s'%eos[label], color='w', size=10)
        ax[idx//5, idx%5].set_xticks([])
        ax[idx//5, idx%5].set_yticks([])
        print(idx, 'finished')
    #plt.imshow(we, extent=extent, origin='lower', interpolation=None, cmap='viridis', aspect='auto')
    #plt.xticks([0.5*np.pi, np.pi, 1.5*np.pi, 2.0*np.pi], [r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$'])
    #plt.xlabel(r'$\phi$')
    #plt.ylabel(r'$p_T\ [GeV]$')
    #plt.colorbar()
    #smash_style.set()
    plt.savefig('we_16_events.png')
    plt.show()

def avg_weight_evidence(nevent=1000):
    '''highlight the most important pixels for EoS classification'''
    x_test = classifier.x_test
    mask = sampler.MarginalSampler(x_test)
    avg_we = np.zeros(shape=(classifier.height, classifier.width))
    we_eosl = []
    we_eosq = []
    #nevent = len(x_test)
    for idx in xrange(nevent):
        x0 = x_test[idx]
        label, we = weight_of_evidence(x0, mask)
        avg_we += we / nevent
        if label == 0:
            we_eosl.append(we)
        else:
            we_eosq.append(we)
        print(idx, 'finished')
    avg_we_eosl = np.mean(np.array(we_eosl), axis=0)
    avg_we_eosq = np.mean(np.array(we_eosq), axis=0)
    np.savetxt('avg_we.dat', avg_we)
    np.savetxt('avg_we_eosl.dat', avg_we_eosl)
    np.savetxt('avg_we_eosq.dat', avg_we_eosq)


if __name__ == '__main__':
    plot_weight_evidence()
    #avg_weight_evidence(100)
