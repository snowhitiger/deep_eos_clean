#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Mon 19 Dec 2016 03:44:24 PM CET

import matplotlib.pyplot as plt
import numpy as np
from four_momentum import PT, PHI, NPT, NPHI
import scipy

skip_npt = 0

# flip left right for phi direction to double the training dataset
def flip_left_right(x_dat):
    x_flip = np.copy(x_dat)
    def flip(spec1d):
        spec2d = spec1d.reshape(NPT - skip_npt, NPHI)
        return spec2d[:, ::-1].flatten()
    return np.apply_along_axis(flip, arr=x_flip, axis=1)


#### shift 1D azimuthal angle distribution with dphi
def __shift_1d(spec_at_1pt, dphi=0.25*np.pi):
    angles = np.concatenate([PHI-2*np.pi, PHI, PHI+2*np.pi]) + dphi
    specs =  np.concatenate([spec_at_1pt, spec_at_1pt, spec_at_1pt])
    flinear = scipy.interpolate.interp1d(angles, specs)
    return flinear(PHI)

def random_shift(x_dat):
    x_shift = x_dat
    ### - pi/8 < dphi < pi/8
    def shift_spec(spec1d):
        dphi = (np.random.uniform(0.0, 1.0) - 0.5) * 0.25 * np.pi
        func = lambda x: __shift_1d(x, dphi)
        spec2d = spec1d.reshape(NPT - skip_npt, NPHI)
        shifted = np.apply_along_axis(func, arr=spec2d, axis=1)
        return shifted.flatten()
    return np.apply_along_axis(shift_spec, arr=x_shift, axis=1)


if __name__=='__main__':
    dat = np.loadtxt('dat/dY_spec_auau200_pbpb2760_ideal_nz51.dat')

    dat_lr = flip_left_right(dat[:10])

    event_id = 1
    pt_bin = 3

    plt.plot(PHI, dat[event_id].reshape(15, 48)[pt_bin], label='origin')
    
    plt.plot(PHI, dat_lr[event_id].reshape(15, 48)[pt_bin], label='flip')

    shift_dat = random_shift(dat[:10])
    plt.plot(PHI, shift_dat[event_id].reshape(15, 48)[pt_bin], label='shift')

    smash_style.set()

    plt.legend(loc='best')
    plt.xlabel(r'$\Phi$', alpha=0.8)
    plt.ylabel(r'$\rho(p_T^{fix}, \Phi)$', alpha=0.8)
    plt.xticks([0, np.pi, 2*np.pi], [r'$0$', r'$\pi$', r'$2\pi$'])
    plt.yticks([])
    plt.xlim(0, 2*np.pi)

    plt.subplots_adjust(bottom=0.15)
    plt.savefig('dat_augmentation.pdf')

    plt.show()

