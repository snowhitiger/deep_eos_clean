#/usr/bin/env python
#author: lgpang
#email: lgpang@qq.com
#createTime: Wed 12 Oct 2016 02:38:18 PM CEST

import numpy as np
from keras.initializations import normal
from keras.layers.advanced_activations import LeakyReLU, PReLU, ELU
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Input, Flatten
from keras.optimizers import SGD, Adam, Adamax
from keras.constraints import maxnorm
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import AveragePooling2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint
from keras.layers.normalization import BatchNormalization
from keras.models import model_from_json
from keras.regularizers import l2, activity_l2, l1

from keras.callbacks import History 

from four_momentum import NPT, NPHI
from augmentation import skip_npt

seed = 13
np.random.seed(seed)

##### load training and testing dataset
import spec
import pandas as pd

x_train, y_train = spec.load_data('data/training_data.csv', lr_flip=True)
n_train = len(x_train)

x_test, y_test = spec.load_data('data/test_ipglasma.csv')
n_test = len(x_test)

height, width = NPT-skip_npt, NPHI

print('shape=', x_train[0].shape)

x_train = x_train.reshape(n_train, height, width, 1).astype('float32')
x_test = x_test.reshape(n_test, height, width, 1).astype('float32')

nb_classes = 2
##### define the layer stacks

model = Sequential()

# used for LeakyReLU
leaky_alpha = 0.3

##### dim_ordering='tf', shape=(height, width, 1)
##### dim_ordering='th', shape=(1, height, width)

model.add(Convolution2D(16, 8, 8, border_mode='same',
                        input_shape=(height, width, 1),
                        init='normal',
                        W_regularizer=l2(0.01))
                        )

model.add(BatchNormalization(mode=0, axis=3))
model.add(PReLU())
model.add(Dropout(0.2))

model.add(Convolution2D(32, 7, 7,
                border_mode='same',
                init='normal',
                W_regularizer=l2(0.01)))

model.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='same', dim_ordering='default'))
model.add(BatchNormalization(mode=0, axis=3))
model.add(PReLU())
model.add(Dropout(0.2))

model.add(Flatten())
model.add(Dense(128, W_regularizer=l2(0.01), init='normal'))
model.add(BatchNormalization(mode=0))
model.add(Activation('sigmoid'))
model.add(Dropout(0.5))

model.add(Dense(nb_classes))
model.add(Activation('softmax'))

adm = Adamax(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=1.0E-6)

model.compile(loss='categorical_crossentropy',
              optimizer=adm,
              metrics=['accuracy'])

checkpointer = ModelCheckpoint(filepath="ckpt/weights.hdf5", verbose=1, save_best_only=True)

history = History()

model.fit(x_train, y_train,
          validation_split=0.1,
          nb_epoch=500, batch_size=64,
          callbacks=[checkpointer, history])

err, acc = model.evaluate(x_test, y_test, batch_size=1)
print('predict err=%s, acc=%s'%(err, acc))


model_json = model.to_json()
with open("ckpt/model.json", "w") as json_file:
    json_file.write(model_json)

# later ...
# load json and create model
with open('ckpt/model.json', 'r') as json_file:
    loaded_model_json = json_file.read()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("ckpt/weights.hdf5")

loaded_model.compile(loss='categorical_crossentropy',
              optimizer=adm,
              metrics=['accuracy'])

err, acc = loaded_model.evaluate(x_test, y_test, batch_size=1)
print('predict err=%s, acc=%s'%(err, acc))

hist_loss, val_loss, hist_acc, val_acc = history.history['loss'], history.history['val_loss'], history.history['acc'], history.history['val_acc']
np.savetxt('train_history_1.txt', np.array([hist_loss, val_loss, hist_acc, val_acc]).T)
